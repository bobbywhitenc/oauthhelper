package com.bolt.web.oauth;

import java.io.IOException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Enumeration;



import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.bolt.connect.spi.Credentials;
import com.bolt.connect.spi.exception.ExpiredCredentialsException;
import com.bolt.connect.spi.exception.NotAuthorizedException;
import com.bolt.connect.spi.exception.SystemErrorException;
import com.bolt.connect.spi.exception.SystemNonRecoverableException;
import com.bolt.connect.spi.oauth2.OAuthClient;

/**
 * Servlet implementation class OAuthHelper
 */
@WebServlet(description = "Initiates OAuth 2.0 Authentication and handles response", urlPatterns = { "/OAuthHelper" })
public class OAuthHelper extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final String CLIENT_ID = "pwl0mx54tz0b9ed51n61tonqhme3e803";
	@SuppressWarnings("unused")
	private static final String CLIENT_SECRET = "DJCrOJjnsAreMxUeGu2eVKwMdVhpDBsh";
	private static final String OAUTH_AUTHORIZE_URL = "https://api.box.com/oauth2/authorize";
 
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public OAuthHelper() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		dispatchRequest(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		dispatchRequest(request, response);
	}
	
	/**
	 * Dispatch the Request to the correct handler
	 * 
	 * @param request
	 * @param response
	 * @throws  
	 */
	protected void dispatchRequest(HttpServletRequest request, HttpServletResponse response) {
		
		System.out.println("--------------Request Received-----------");
		System.out.println(request.getContextPath());
		
		try {
			if (request.getParameter("code") != null 
					&& request.getParameter("state") != null) {
				// Handles the callback After the User approves OAuth request
				processOAuthCodeRequest(request,response);
			} else if (request.getParameter("login")!=null){
				// Displays the Start Page to begin the OAuth process
				processStartPage(request,response);
			} else {
				//echoRequest(request,response);
				processStartPage(request,response);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	/**
	 * Echo Request Parameters back to the caller as Text and the console
	 * 
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	protected void echoRequest(HttpServletRequest request, HttpServletResponse response) throws IOException  {
		
		PrintWriter client = response.getWriter();

		client.println(request.getMethod().toString());

		@SuppressWarnings("unchecked")
		Enumeration<String> params = request.getParameterNames();

		client.println("---------URL Form Parameters---------");
		while (params.hasMoreElements()) {
			String thisParamName = params.nextElement();
			String thisParamValue = request.getParameter(thisParamName);
			String msg = "{" + thisParamName + " : " + thisParamValue + "}";
			System.out.println(msg);
			client.println(msg);
			
		}
		
		@SuppressWarnings("unchecked")
		Enumeration<String> headers = request.getHeaderNames();
		
		client.println("---------Http Header Parameters---------");
		while (headers.hasMoreElements()) {
			String thisHeaderName = headers.nextElement();
			String thisHeaderValue = request.getHeader(thisHeaderName);
			String msg = "{" + thisHeaderName + " : " + thisHeaderValue + "}";
			System.out.println(msg);
			client.println(msg);
			
		}
		
		@SuppressWarnings("unchecked")
		Enumeration<String> attrNames = request.getAttributeNames();
		client.println("---------Http Attributes ---------");
		while (attrNames.hasMoreElements()) {
			String attrName = attrNames.nextElement();
			String attrValue = request.getAttribute(attrName).toString();
			String msg = "{" + attrName + " : " + attrValue + "}";
			System.out.println(msg);
			client.println(msg);
			
		}
		
		
		client.println("---------Remote Address ---------------------");
		client.println(request.getRemoteAddr());
		System.out.println("Remote Address="+request.getRemoteAddr());
		
		client.println("---------Beginning of Content ---------------------");
		System.out.println("---------Beginning of Content ---------------------");
		
		ServletInputStream is = request.getInputStream();
		byte[] buffer = new byte[1024];
		while (is.available() > 0) {
			int len = is.read(buffer);
			String decoded = new String(buffer, 0, len, "UTF-8");
			client.println(decoded);
			System.out.println(decoded);
		}
		client.println("---------End of Content ---------------------");
		System.out.println("---------End of Content ---------------------");

		
	}
	
	/**
	 * OAuth2 Authorization Response includes state info and code
	 * 
	 * @param request
	 * @param response
	 * @throws IOException 
	 */
	protected void processOAuthCodeRequest(HttpServletRequest request, HttpServletResponse response) throws IOException  {
		
		String stateValue=request.getParameter("state");
		String codeValue=request.getParameter("code");
		
		PrintWriter out = response.getWriter();
		out.println("<html><head><title>Process OAuth Code Result</title></head><body>");
		out.println("<h1>Accepting OAuth Code message - After Authorization</h1>");
		out.println("<table><tr><td><b>state</b></td><td>"+stateValue+ "</td><tr>");
		out.println("<tr><td><b>code</b><td>"+codeValue+"</td><tr></table>");
		
		out.println("<p><li>Requesting OAuth Token</li></ul></p>");
		
		Credentials creds = null;
		try {
			creds = OAuthClient.getAuthTokenUsingCode(codeValue);
		} catch (ExpiredCredentialsException e) {
			e.printStackTrace();
		} catch (NotAuthorizedException e) {
			e.printStackTrace();
		} catch (SystemErrorException e) {
			e.printStackTrace();
		} catch (SystemNonRecoverableException e) {
			e.printStackTrace();
		}
		
		out.println("<ul><li>"+creds.toString()+"</li><ul>");
		out.println("<br><br>"+this.generateBoxAuthorizeLink());
		out.println("</body></html>");
		
		OAuthClient.saveCredentials(creds, "latest.creds");
		
		
	
	}
	
	/**
	 * Helper method to build a complete URL.  Handles adding the seperators and encoding
	 * 
	 * @param url the base URL
	 * @param paramName the parameter name
	 * @param value the parameter value
	 * @return
	 */
	protected String appendURLParameter(StringBuffer theUrl, String paramName,String value)  {
		theUrl.append(theUrl.toString().contains("?") ? '&' : '?' );
		theUrl.append(paramName);
		theUrl.append('=');
		try {
			theUrl.append(URLEncoder.encode(value,"UTF-8"));
		} catch (UnsupportedEncodingException e) {
			theUrl.append(value);
		}
		return theUrl.toString();
	}
	
	/**
	 * Default Request Simply Displays a link to allow the user to authenticate with Box
	 * @param request
	 * @param response
	 * @throws IOException 
	 * @throws Throwable 
	 */
	protected void processStartPage(HttpServletRequest request, HttpServletResponse response) throws IOException  {
	//	System.out.println("DEBUG="+this.getServletName()+"Start Page");
		
		String theLinkTag = generateBoxAuthorizeLink();
		
		PrintWriter out = response.getWriter();
		out.println("<HTML><HEAD><TITLE>OAuth2 Authorization Start Page</TITLE></HEAD><BODY>");
		out.println("<h1>OAuth2 Authorization Start Page Utility</h1>");
		out.println("<p>Click on the link below to begin OAuth with Box.com</p>");
		out.println(theLinkTag);
		out.println("</BODY></HTML>");
		
	}

	protected String generateBoxAuthorizeLink() {
		// Embed a link to initiate the OAuth Sequence
		StringBuffer theQuery= new StringBuffer(OAUTH_AUTHORIZE_URL);
		this.appendURLParameter(theQuery,"response_type","code");
		this.appendURLParameter(theQuery,"client_id",CLIENT_ID);
		this.appendURLParameter(theQuery,"state","Box");
		this.appendURLParameter(theQuery,"redirect_uri","http://localhost:8080/OAuthHelper");
		
		StringBuffer theLinkTag = new StringBuffer();
		theLinkTag.append("<a href=\"");
		theLinkTag.append(theQuery);
		theLinkTag.append("\">");
		theLinkTag.append("Authorize Box Connector</a>");
		return theLinkTag.toString();
	}


}
